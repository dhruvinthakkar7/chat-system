
import java.net.*;
import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;

public class ServerSend extends Thread{
		Socket socket;
                static ArrayList<String> mssgs = new ArrayList<>();
		static String echoString="";
                String name="";
                int count = 0;
                int preCount = 0;
                boolean flag;
		ServerSend(String name,Socket socket,boolean flag){
			this.socket = socket;
			this.name = name;
                        this.flag = flag;
		}
		public void run(){
					try{
								PrintWriter output = new PrintWriter(socket.getOutputStream(),true);
									do{
                                                                            if(count>preCount && !flag){
                                                                                preCount = count;
//                                                                                System.out.println(echoString+" "+name);
                                                                                if(ServerCode.getSendingSocket(name) == socket && !echoString.equals("")){
                                                                                                        System.out.println(echoString);
                                                                                                        output.println(echoString);
                                                                                }
                                                                            }
                                                                         else if(count>preCount && flag){
                                                                                preCount = count;
                                                                                if(ServerCode.getSendingSocket(name) == socket){
                                                                                    for(String msg: mssgs)
                                                                                               output.println(msg);
                                                                                    mssgs.clear();
                                                                                }
                                                                              }
    								}while(!echoString.equalsIgnoreCase("exit"));

 
					}catch(IOException e){}
					finally{
						try{
						socket.close();
						}
						catch(Exception e){
						System.out.println(e);
					}
				}
		}

    
}
