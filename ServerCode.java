

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class ServerCode {
    static Map<String,Socket> users = new HashMap<>();
    static String name="",myname="";
    static Socket socket,sendSocket;
    static ArrayList<String> offlineUsers = new ArrayList<>();
    public static Socket getSendingSocket(String name){
            if(users.containsKey(name))
                    return users.get(name);
            else{
                  System.out.println(name+" doesnt exist!");
                 if(!offlineUsers.contains(name) && !name.equals("Broadcast"))
                             offlineUsers.add(name);
                    return null;
            }
    }
    public static void main(String args[]) throws ClassNotFoundException{
    	try(ServerSocket serverSocket = new ServerSocket(5000)){
            while(true){
        	   socket = serverSocket.accept();
                   BufferedReader input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                   Scanner scanner = new Scanner(socket.getInputStream());
                   myname = input.readLine();
                   if(offlineUsers.contains(myname)){
                       offlineUsers.remove(myname);
                       ServerSend s = new ServerSend(myname,socket,true);
                       s.start();
                       s.count++;

                   }
                   users.put(myname,socket);
                   for(String key : users.keySet()){
                       if(!key.equals(myname) && key!=null){
                           System.out.println(key);
                       }
                   }
                   Echore e = new Echore(socket);
                   e.start();
        	   System.out.println("Client Connected!");
    	}
       }
    	catch(IOException e){
    		System.out.println("Server Exception:"+e.getMessage());
    	}
    }
}
