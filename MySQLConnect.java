
import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import javax.swing.JOptionPane;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author hp
 */
public class MySQLConnect {
    private static final String CONNECTION_STRING = "jdbc:sqlite:"+ new File("").getAbsolutePath() + "\\src\\users.db";
//    private static final String USERNAME = "Dhruvin";
//    private static final String PASSWORD = "dt";
    /**
     * 
     * @return Connection
     * Returns a valid connection to employee management DB if it can,otherwise it returns null.
     */
    public static Connection getConnection(){
        Connection conn = null;
        try{
            conn = DriverManager.getConnection(CONNECTION_STRING);
            JOptionPane.showMessageDialog(null,"Connection Established!");
        }catch(SQLException ex){
            JOptionPane.showMessageDialog(null,"Connection Failed:"+ ex.getMessage());
        }
        return conn;
    }
    
    
}
    