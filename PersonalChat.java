import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
public class PersonalChat extends JFrame implements ActionListener{
    public JLabel lblName = new JLabel();
    public JButton sendBtn;
    public JPanel jp3;
    public JTextField jtfMsg = new JTextField(50);
	PersonalChat(String title){
                        setTitle(title);
			setSize(700,700);
			JPanel jp = new JPanel();
			jp.setBackground(Color.BLUE);
			jp.add(lblName);
			add(jp,BorderLayout.NORTH);
			JPanel jp1 = new JPanel();
			jp1.setLayout(new FlowLayout());
			jp1.add(jtfMsg);
			jp1.setLayout(new FlowLayout(FlowLayout.RIGHT));
			sendBtn = new JButton("SEND");
                        sendBtn.addActionListener(this);
			jp1.add(sendBtn);
			JPanel jp2 = new JPanel();
			jp2.setLayout(new FlowLayout(FlowLayout.CENTER));
			jp2.add(jp1);
			add(jp2,BorderLayout.SOUTH);
                        jp3 = new JPanel();
                        jp3.setBackground(Color.WHITE);
                        add(jp3);
			setVisible(true);
	}
    @Override
	public void actionPerformed(ActionEvent ae){
	Send.echoString = jtfMsg.getText();
        jtfMsg.setText("");
        Send.name = lblName.getText();
        if(lblName.getText().equals("Broadcast")){
            Send.myname = Login.txtUsername.getText();
        }
        Send.count++;
	}
}