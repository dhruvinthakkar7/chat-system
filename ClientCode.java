
import java.io.*;
import java.net.*;
public class ClientCode extends Thread{
         String name="";
         ChatSystem cs;
        ClientCode(String name,ChatSystem cs){
            this.name = name;
            this.cs = cs;
        }
        public void run(){
            try(Socket socket = new Socket("localhost",5000)){ 
                            System.out.println("client");
                            PrintWriter output = new PrintWriter(socket.getOutputStream(),true);
                            output.println(name);
                            Send s = new Send(socket);
                            Receive r = new Receive(socket,name);
                            s.start();
                            r.start();
                            while(s.isAlive() || r.isAlive());
    		
    	   }
        catch(SocketTimeoutException e){
            System.out.println("SO time out Exception");
        }
    	catch(IOException e){
    		System.out.println("IO Exception"+e);
    	}
       
      }
}
